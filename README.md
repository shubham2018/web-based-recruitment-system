# Web based recruitment system

The project is aimed at developing an application for the "ONLINE PLACEMENT
AUTOMATION SYSTEM" of the company which is web-based and provides feature of central Recruitment Process System for a company.The system is an application that can be accessed
and effectively used throughout the organization with proper login enabled. This system can be
used for proper interaction between Recruitment department (HR) in the company and
candidates information with regard to placement. In this Company should able to upload their
details, requirement, job details and candidate logging should be able to upload their personal
and educational information. Our project provides the facility of maintaining the details of the candidates and
company. It reduces the manual work and consumes less paper work to reduce the time.

## Modules in this project:

1. Administrator

2. Jobseekers/Candidate

3. Company

## Brief description on the modules:

1. Administrator:

     Administrator has the full authority over the website. He can view all the registered users and have the power to delete them. He can edit the web pages and update them. He can view all the company details also.

2. Jobseeker/Candidate:

     A jobseeker can register himself. After registration, he will be directed to his homepage. Here he can update his profile, change password and see the examination details and all.

3. Company:

     A company can register itself, conduct online examination, approve or disapprove candidates attending examination and provides results about the selected candidates.

## websites refrerred:
naukari.com,  
indeed.co.in,  
linkedin.com,  
monsterindia.com
